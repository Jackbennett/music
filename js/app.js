document.addEventListener('DOMContentLoaded', function () {
  var myChart = audioObj(document.getElementById('display'))
  var trackSelect = document.getElementById('track')
  var audioElement = document.getElementById('player')

  trackSelect.addEventListener('change', loadAudio)
  audioElement.addEventListener('canplay', myChart.create)
  audioElement.addEventListener('play', myChart.paint)
  audioElement.addEventListener('pause', myChart.stop)

  function loadAudio(input) {
    if (input.target.files.length === 0) {
      console.warn('input changed but no valid file present')
      return false
    }
    audioElement.src = URL.createObjectURL(input.target.files[0])
    audioElement.addEventListener(
      'canplay',
      (evt) => {
        evt.target.play()
      },
      { once: true }
    )
  }
})
var audioObj = function (display) {
  var self = this
  self.width = display.clientWidth
  self.height = display.clientHeight
  return {
    create: function (evt) {
      if (!self.chart) {
        self.chart = equalizer(self.width, self.height)

        var ctx = new AudioContext()
        var source = ctx.createMediaElementSource(evt.target)
        var analyser = ctx.createAnalyser()

        analyser.fftSize = 64
        var frequencyData = new Uint8Array(analyser.frequencyBinCount)

        // connections
        source.connect(analyser)
        analyser.connect(ctx.destination)

        self.analyser = analyser
        self.frequencyData = frequencyData
      }
    },
    paint: function d() {
      self.frameID = requestAnimationFrame(d)
      self.analyser.getByteFrequencyData(self.frequencyData)
      self.chart.update(self.frequencyData)
    },
    stop: function () {
      cancelAnimationFrame(self.frameID)
    },
  }
}

var equalizer = function (w, h) {
  var size = d3.scale.linear().domain([0, 255]).range([0, 1])

  var color = d3.scale
    .linear()
    .domain([0, 255])
    .range(['#078080', '#f45d48'])
    .interpolate(d3.interpolateHsl)
    .clamp(true)

  var background = d3.scale
    .linear()
    .domain([0, 10, 128, 255])
    .range(['#E7EDEA', '#ffc811', '#ff8811', '#ff1f01'])

  var svg = d3
    .select('#display')
    .append('svg')
    .attr('width', w)
    .attr('height', h)
    .append('g')
    .attr('transform', function () {
      return 'translate(' + (w - w / 2) + ')'
    })

  function update(data) {
    var barHeight = Math.floor(h / 32)
    var bar = svg.selectAll('rect').data(data)

    bar
      .enter()
      .append('rect')
      .attr('y', function (d, i) {
        return i % 2 ? barHeight * (i - 1) : barHeight * i
      })
      .attr('width', function (d) {
        return w / 2
      })
      .attr('height', function (d) {
        return barHeight * 2 + 'px'
      })

    bar
      .attr('transform', function (d, i) {
        if (i % 2) {
          return 'scale(' + size(d) * -1 + ', 1)'
        }
        return 'scale(' + size(d) + ', 1)'
      })
      .attr('fill', function (d) {
        return color(d)
      })

    bar.exit().remove()
  }

  return {
    update: update,
  }
}
